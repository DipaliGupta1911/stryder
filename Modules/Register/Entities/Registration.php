<?php

namespace Modules\Register\Entities;

use Illuminate\Database\Eloquent\Model;
// use Illuminate\Database\Eloquent\Factories\HasFactory;

class Registration extends Model
{
   // use HasFactory;

    protected $table = "registration";
    protected $primaryKey = "id";
    protected $fillable = ['bike_type','birthdaytime','retail_name','retail_city','product_name','product_price','personal_retail_name','email','gender','age','phone','state','city','address','pincode','comment','invoice_file'];
    
    protected static function newFactory()
    {
        return \Modules\Register\Database\factories\RegistrationFactory::new();
    }
}
