<?php

namespace Modules\Register\Http\Controllers;

use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Modules\Register\Entities\Registration;
use Storage;
class RegisterController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Renderable
     */
    public function index(Request $request)
    {
        if ($request->hasFile('myfile')) {
            if(isset($request['email'])){

                $email = $request['email'];
                $invoice_file = $request->file('myfile');
                //$filename   = $email . '.' . $invoice_file->getClientOriginalExtension();
                $originalFileName = $invoice_file->getClientOriginalName();
                $name = $originalFileName.'_'.$email;

                $response = Storage::makeDirectory('public/invoice/'.$email);
                $fileName = $originalFileName.'_'.date('Y-m-d-H-i-s');
                $path = $invoice_file->storeAs('public/invoice/'.$email.'/', $fileName);

                $registration = new Registration;
                $registration->bike_type = $request['bike_type'];
                $registration->birthdaytime = $request['birthdaytime'];
                $registration->retail_name = $request['retail_name'];
                $registration->retail_city =  $request['retail_city'];
                $registration->product_name = $request['product_name'];
                $registration->product_price = $request['product_price'];
                $registration->personal_retail_name = $request['personal_retail_name'];
                $registration->email = $email;
                $registration->gender = $request['gender'];
                $registration->age = $request['age'];
                $registration->phone = $request['number'];
                $registration->state = $request['state'];
                $registration->city = $request['city'];
                $registration->address = $request['address'];
                $registration->pincode = $request['pincode'];
                $registration->comment = $request['Comment'];
                $registration->invoice_file = $fileName;

                $registration-> save();

                return response(['success'=>true,'message'=>'Your Data Succesfully saved']);
            }
            response()->json(['success'=>false,'message'=>'Errors']);
        }
            response()->json(['success'=>false,'message'=>'Errors']);

        
    }

    /**
     * Show the form for creating a new resource.
     * @return Renderable
     */
    public function create()
    {
        return view('register::create');
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Renderable
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Renderable
     */
    public function show($id)
    {
        return view('register::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Renderable
     */
    public function edit($id)
    {
        return view('register::edit');
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Renderable
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Renderable
     */
    public function destroy($id)
    {
        //
    }
}
