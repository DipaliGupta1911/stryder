<nav class="hk-nav hk-nav-light">
    <a href="javascript:void(0);"  class="hk-nav-close"><span class="feather-icon"><i data-feather="x"></i></span></a>
    <div class="nicescroll-bar">
        <div class="navbar-nav-wrap">
            <ul class="navbar-nav flex-column">
                <li class="nav-item">
                    
                    <a class="nav-link" href="{{route('stores.index',['type'=>'all']) }}" >
                        <i>@include('svg.sonycenter')</i>
                        <span class="nav-link-text">Store Master</span>
                    </a>
                </li>
                <li class="nav-item ">
                    <a class="nav-link" href="javascript:void(0);" data-toggle="collapse" data-target="#username">
                        <i>@include('svg.loginuser')</i>
                        <span class="nav-link-text">{{Auth::guard('admin')->user()->name}}</span>
                    </a>
                    <ul id="username" class="nav flex-column collapse collapse-level-1">
                        <li class="nav-item">
                            <ul class="nav flex-column">
                                <li class="nav-item @if(Request::segment(2) == 'password') active @endif">
                                        <a class="nav-link" href="{{route('password.changed') }}" >
                                            <i class="fas fa-boxes"></i>
                                            <span class="nav-link-text">Change Password</span>
                                        </a>
                                    </li>
                                <li class="nav-item">
                                    <ul class="nav flex-column">
                                        <li class="nav-item">
                                            <a href="{{route('admin.logout')}}" class="nav-link" href="alerts.html">
                                                 <i>@include('svg.logout')</i>
                                                <span class="nav-link-text">Logout</span>
                                            </a>
                                        </li>
                                    </ul>
                                </li>    
                            </ul>
                        </li>
                    </ul>
                </li>
            </ul>   
        </div>
    </div>
</nav>
<div id="hk_nav_backdrop" class="hk-nav-backdrop"></div>