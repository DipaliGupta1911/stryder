<div class="modal fade" id="custom_modal" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
        	<div class="modal-header">
                <h5 class="modal-title" id="modal_title">CUSTOMER ADDRESS</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body" id="modal_body">
			</div>
			<div class="text-right">
                <button type="button" class="btn btn-primary mr-10" data-dismiss="modal" title="close">Close</button>
            </div>   	  
        </div>
    </div>
</div>