<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Registration extends Model
{
    protected $table = "registration";
    protected $primaryKey = "id";
    protected $fillable = ['bike_type','retail_name','retail_city','product_name','product_price','gender','age','phone','state','city','address','pincode','comment','invoice_file','created_at','updated_at'];
}
