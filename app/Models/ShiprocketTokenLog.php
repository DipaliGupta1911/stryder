<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ShiprocketTokenLog extends Model
{
    protected $table = 'shiprocket_token_logs';
    protected $fillabale = ['token'];

    public $timestamp = true;
}
