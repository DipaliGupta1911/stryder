<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\GeneralEnquiry;
use App\Models\DealershipEnquiry;
use Log;

class EnquiryController extends Controller
{
    public function generalEnquiry(Request $request){
        $request_data = $request->all();
        $general_enquiry = GeneralEnquiry::firstOrNew(['phone_number'=>$request_data['contact']['Phone number']]);
        $general_enquiry->name = $request_data['contact']['Name'];
        $general_enquiry->phone_number = $request_data['contact']['Phone number'];
        $general_enquiry->email = $request_data['contact']['email'];
        $general_enquiry->state_name = $request_data['state'];
        $general_enquiry->pincode = $request_data['pincode'];
        $general_enquiry->comment = $request_data['contact']['Comment'];
        $general_enquiry->save();
        //print_r($general_enquiry);
        return response(['success'=>true,'message'=>'Your Data Succesfully saved']);
    }

    public function dealershipEnquiry(Request $request){
      
        $request_data = $request->all();
        $general_enquiry = DealershipEnquiry::firstOrNew(['phone_number'=>$request_data['contact']['Phone number']]);
        $general_enquiry->name = $request_data['contact']['Name'];
        $general_enquiry->phone_number = $request_data['contact']['Phone number'];
        $general_enquiry->email = $request_data['contact']['email'];
        $general_enquiry->entity_name = $request_data['entity_name'];
        $general_enquiry->state_name = $request_data['state'];
        $general_enquiry->pincode = $request_data['pincode'];
        $general_enquiry->comment = $request_data['contact']['Comment'];
        $general_enquiry->save();
        //print_r($general_enquiry);
        return response(['success'=>true,'message'=>'Your Data Succesfully saved']);
    }
}
