<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Custom\StoreLocatorController;


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });

Route::get('/', function () {     return view('welcome'); })->middleware(['auth.shopify'])->name('home');


Route::get('test',function(){
    $html = "Gautam";
    return response($html)->withHeaders(['Content-Type' => 'application/json']);
});

Route::get('storelocators/{state_name?}/{city_name?}',[StoreLocatorController::class,'listingNew']);

Route::get('storelocators/{state}/{city}/{slug}',[StoreLocatorController::class,"MoreDetail"])->name('custom.storeLocator.detail');

Route::post('/getstore-name',[StoreLocatorController::class,'getStoreName'])->name('StoreName.get');

Route::post('/getstore-data',[StoreLocatorController::class,'getStoreDetail'])->name('StoreDetail.get');

Route::get('/getstore-all',[StoreLocatorController::class,'getStoreDetailAll'])->name('StoreDetail.getAll');

